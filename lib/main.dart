// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, use_key_in_widget_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wheater_app/climatempo.dart';

void main() {
  runApp(WheaterApp());
}

const titleStyle = TextStyle(
  color: Colors.white,
  fontSize: 14,
  fontWeight: FontWeight.w500,
);

class ForecastData {
  final String time;
  final String image;
  final int temperature;

  ForecastData(this.time, this.image, this.temperature);
}

List<ForecastData> forecasts = [
  ForecastData('Now', 'sol', 20),
  ForecastData('11AM', 'sol', 20),
  ForecastData('12AM', 'nublado', 20),
  ForecastData('Now', 'sol', 20),
  ForecastData('11AM', 'chuva', 20),
  ForecastData('12AM', 'nublado', 20),
  ForecastData('12AM', 'nublado', 20),
];

class WheaterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: FutureBuilder<Map<String, dynamic>>(
          future: const ClimaTempo().getWeather(3693),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return const CircularProgressIndicator();
            }

            return Scaffold(
              backgroundColor:
                  snapshot.data!['data']['condition'] == 'Ensolarado'
                      ? Color(0xFF255AF4)
                      : Color.fromARGB(255, 55, 60, 75),
              body: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Text(
                    snapshot.data!['name'],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 28,
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                          'images/128px/${snapshot.data!['data']['icon']}.png',
                          width: 96,
                          height: 96),
                      Text(
                        snapshot.data!['data']['condition'],
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 28,
                        ),
                      ),
                      Text(
                        "${snapshot.data!['data']['temperature']}°C",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 52,
                          fontWeight: FontWeight.w500,
                          shadows: [
                            Shadow(
                              color: Colors.black,
                              offset: Offset(4, 4),
                              blurRadius: 7,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Column(
                        children: [
                          Image.asset('images/umidade.png'),
                          Text("Humidity", style: titleStyle),
                          Text("${snapshot.data!['data']['humidity']}%",
                              style: titleStyle),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset('images/vento.png'),
                          Text("Wind", style: titleStyle),
                          Text("${snapshot.data!['data']['wind_velocity']}km/h",
                              style: titleStyle),
                        ],
                      ),
                      Column(
                        children: [
                          Image.asset('images/sensacao.png'),
                          Text("Feels like", style: titleStyle),
                          Text("${snapshot.data!['data']['sensation']}°C",
                              style: titleStyle),
                        ],
                      ),
                    ],
                  ),
                  FutureBuilder<List<Map<String, dynamic>>>(
                      future: const ClimaTempo().getForecast(3693),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return CircularProgressIndicator();
                        }

                        if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }

                        return Container(
                          height: 90,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: snapshot.data!
                                .where((f) =>
                                    DateTime.parse(f['date']).hour >=
                                    DateTime.now().hour)
                                .map((f) => Forecast(f['date'], "sol",
                                    f['temperature']['temperature']))
                                .toList(),
                          ),
                        );
                      }),
                ],
              ),
            );
          }),
    );
  }
}

class Forecast extends StatelessWidget {
  final String time;
  final String image;
  final int temperature;
  late DateTime date;

  Forecast(this.time, this.image, this.temperature) {
    this.date = DateTime.parse(time);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 39),
      child: Column(
        children: [
          Text(
            DateFormat('hh a').format(date),
            style: TextStyle(color: Colors.white),
          ),
          // Image.asset(
          //   'images/$image.png',
          //   width: 36,
          //   height: 36,
          // ),
          Text(
            "$temperature°C",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    );
  }
}
