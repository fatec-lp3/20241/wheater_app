import 'package:http/http.dart' as http;
import 'dart:convert';

class ClimaTempo {
  const ClimaTempo();

  final String baseUrl = 'https://apiadvisor.climatempo.com.br';
  final String token = 'f37080f891482f9a7918d485419845a7';

  Future<Map<String, dynamic>> getWeather(int city) async {
    String url = '$baseUrl/api/v1/weather/locale/$city/current?token=$token';
    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode != 200) {
      throw response.body;
    }

    return json.decode(response.body);
  }

  Future<List<Map<String, dynamic>>> getForecast(int city) async {
    String url = '$baseUrl/api/v1/forecast/locale/$city/hours/72?token=$token';
    http.Response response = await http.get(Uri.parse(url));
    if (response.statusCode != 200) {
      throw response.body;
    }

    var lista = json.decode(response.body)['data'] as List<dynamic>;
    var forecasts = lista.map((e) => e as Map<String, dynamic>).toList();
    return forecasts;
  }
}
